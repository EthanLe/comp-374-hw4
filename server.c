#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>


#define BUFFER 256
int main(void)
{
	puts("Server");
	
	char str[BUFFER];
	int f1, f2;
	mkfifo("SERVER", 0666);
	mkfifo("CLIENT", 0666);
	
	while(1)
	{
		printf("waiting...\n");
		f1 = open("SERVER", O_WRONLY);
		f2 = open("CLIENT", O_RDONLY);
		
		
		read(f2, str, BUFFER);
		printf("Client: %s\n", str);
		printf("You: ");
		fgets(str, BUFFER, stdin);
		write(f1, str, BUFFER);
		close(f1);
		close(f2);
	}		
		
	return EXIT_SUCCESS;
}
