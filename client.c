#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>

#define BUFFER 256
int main(void) {

	int f1,f2;
	char str[BUFFER];
	puts("Client");

	while(1)
	{	
		f1 = open("SERVER", O_RDONLY);
		f2 = open("CLIENT", O_WRONLY);

		printf("You: ");
		fgets(str, BUFFER, stdin);
		write(f2, str, BUFFER);
		printf("waiting...\n");

		read(f1, str, BUFFER);
	
		printf("Server: %s\n", str);
		close(f1);
		close(f2);
			
	}
	return EXIT_SUCCESS;
}
